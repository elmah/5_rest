package ca.polymtl.log4420.model

import net.liftweb._
import common.Box
import json.JsonAST.{JArray, JValue}
import mongodb.record._
import field.{MongoCaseClassField, MongoListField}


class Session private()
  extends BsonRecord[Session]
{
  def meta = Session

  object perriode extends MongoCaseClassField[Session, Perriode]( this )
  object listeDeCours extends MongoListField[Session, Cours]( this )

  implicit def toJson(session : Session): JValue = session.asJValue
  implicit def toJsonFromSeq(sessions : Seq[Session]): JValue = JArray( sessions.map( c => (c.asJValue): JValue ).toList )
  implicit def toJsonFromOption( session: Option[Session] ): Option[JValue] = session.map( _.asJValue )
  implicit def toJsonFromBox( session: Box[Session] ): Box[JValue] = session.map(_.asJValue )
}

object Session extends Session with BsonMetaRecord[Session]

case class Cours( sigle: String )
case class Perriode( saison: Saison, annee: Int )

sealed trait Saison
case object Automne extends Saison
case object Hiver extends Saison
case object Ete extends Saison